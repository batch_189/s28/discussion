// CRUD Operations

// Insert Documents (CREATE)

/**
        Syntax:
            Insert One Document
                -db.collectionName.insertOne({
                    "fieldA": "valueA"
                    "fieldB": "valueB"
                });

        
        Insert Many Documents
            Syntax:
                 -db.collectionName.insertOne([
                    {
                        "fieldA": "valueA"
                        "fieldB": "valueB"
                    },
                    {
                         "fieldC": "valueC"
                        "fieldD": "valueD"
                    }
                 ]);

 */

    db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "Janedoe@gmail.com",
        "department": "none"
    });

    db.users.insertMany([
        {
            "firstName": "Stephen",
            "lastName": "Hawking",
            "age": 76,
            "email": "stephenhawking@gmail.com",
            "department": "none"

        },
        {
            "firstName": "Neil",
            "lastName": "Armstrong",
            "age": 82,
            "email": "armtrong@gmail.com",
            "department": "none"

        }
    ]);

    // 1. Make a new collection with the name "courses"
    //     2. Insert the following fields and values

            db.courses.insertMany([

                {
                    "name": "Javascript 101",
                    "price": 5000,
                    "description": "Introduction to Javascript",
                    "isActive": true
    
                },

                {
                    "name": "HTML 101",
                    "price": 2000,
                    "description": "Introduction to HTML",
                    "isActive": true
                },
               
                {
                    "name": "CSS 101",
                    "price": 2500,
                    "description": "Introduction to CSS",
                    "isActive": false
                }
        ]);

// Find Documents (Read)
/**
        Syntax:
            db.collectionName.find() - this will retrieve all our docs
            db.collectionName.find({"criteria": "value"}) - this will retrieve all documents that will match the criteria
            db.collectionName.fineOne({"criteria": "value"}) - this will return the first doc in out collection that
                match the criteria.
            db.collectionName.findOne({}) - will return the first doc in our collection.

 */

        db.users.find();

        db.users.find({
            "firstName": "Jane"
        });

        db.users.find({
            "lastName": "Armstrong",
            "age": 82
        });

// Updating Documents (Update)
/**
        Syntax:
            db.collectionName.updateOne({
                "criteria": "field"
            },
            {
                $set: {
                    "fieldToBeUpdate": "updateValue"
                }
            });

            db.collection.updateMany(
                {
                    "criteria": "value"
                },
                {
                    $set: {
                        "fieldToBeUpdate": "updateValue"
                    }
                }
            );
 */

        db.users.insertOne({
            "firstName": "test",
            "lastName": "test",
            "age": 0,
            "email": "test@gmail",
            "department": "none"
        });

    // Update One doc
        db.users.updateOne(
        {
            "firstName": "test",
        },
        {
            $set: {
                "firstName": "Bill",
                "lastName": "Gates",
                "age": 65,
                "email": "billgates@gmail",
                "department": "Operations",
                "status": "active"
            }
        }
        
    );

// Removing a field
        
            db.users.updateOne(
                {
                    "firstName": "Bill"
                },
                {
                    $set: {
                        "firstName": "Chen"
                    }
                }
            );

// Updating Multiple Docs
            db.users.updateMany(
                {
                    "department": "none"
                },
                {
                    $set: {
                        "department": "HR"
                    }
                }
            );

            db.users.updateOne(

                {},
                {
                    $set: {
                        "department": "Operations"
                    }
                }
            );

        // Renaming a field
            db.users.updateMany({},
                {
                    $rename: {
                        "department": "dept"
                    }
                });

        // Mini Activity
        // 1. update HTML 101 isActive to false
        // 2. Add new field to all documents, enrollees: 10

            db.courses.updateOne (
                {
                    "name": "HTML 101"
                },
                {
                    $set: {
                        "isActive": false
                    }
                }
            )

            db.courses.updateMany({},
                {
                    $set: {
                        "enrollees": 10
                    }
                }
            );

// Deleting Documents (DELETE)
/**
        Deleting a single doc
            Syntax:
                db.collectionName.deleteOne({"criteria": "value"})
 */

        db.users.deleteOne({
            "firstName": "test"
        });

/**
        Deleting Multiple documents
        Syntax:
            db.collectionName.deleteMany({"criteria": "value"})
 */
            db.users.deleteMany({"dept": "HR"});

            db.courses.deleteMany({}); // deleted all




























